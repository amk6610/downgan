import torch
import torch.nn as nn
import torch.nn.functional as F


class InversePixelShuffle(nn.Module):
    def __init__(self, downscale_factor):
        super().__init__()
        self.downscale_factor = downscale_factor

    def forward(self, x):
        bs = x.size(0)
        c = x.size(1)
        h = x.size(2)
        w = x.size(3)
        out_channel = c * (self.downscale_factor ** 2)
        out_h = h // self.downscale_factor
        out_w = w // self.downscale_factor
        x_view = x.contiguous().view(bs, c, out_h, self.downscale_factor,
                                     out_w, self.downscale_factor)
        x_prime = x_view.permute(0, 1, 3, 5, 2, 4).contiguous().view(
            bs, out_channel, out_h, out_w)
        return x_prime


class DownScalingBlock(nn.Module):
    def __init__(self, in_channels=3):
        super().__init__()
        self.body = nn.Sequential(
            nn.Conv2d(in_channels, 16, 3, 1, 1),
            nn.Conv2d(16, 16, 3, 1, 1),
            InversePixelShuffle(2)
        )

    def forward(self, x):
        out = self.body(x)
        return out


class ResidualBlock(nn.Module):
    def __init__(self, channels=64):
        super().__init__()
        self.body = nn.Sequential(
            nn.Conv2d(channels, channels, 3, 1, 1),
            nn.ReLU(inplace=True),
            nn.Conv2d(channels, channels, 3, 1, 1),
        )

    def forward(self, x):
        out = self.body(x)
        out = out + x
        return out


class TAD_dsn(nn.Module):
    def __init__(self):
        super().__init__()
        self.entry = DownScalingBlock()
        self.residuals = nn.Sequential(
            ResidualBlock(), ResidualBlock(), ResidualBlock())
        self.mid_conv = nn.Conv2d(64, 64, 3, 1, 1)
        self.exit = nn.Conv2d(64, 3, 3, 1, 1)

    def forward(self, x):
        b1 = self.entry(x)
        x = self.residuals(b1)
        x = self.mid_conv(x)
        out = self.exit(x + b1)
        return out
