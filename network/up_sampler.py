import torch.nn as nn
import torch.nn.functional as F


class Upsampler(nn.Module):
    def __init__(self, mode='bicubic'):
        super().__init__()
        self.mode = mode
        if mode == 'conv':
            self.conv = nn.ConvTranspose2d(
                3, 3, kernel_size=5, stride=2, padding=2, output_padding=1)

    def forward(self, lr):
        if self.mode == 'bicubic':
            return F.interpolate(lr, scale_factor=2, mode='bicubic', align_corners=False)
        else:
            return self.conv(lr)
