import torch
import torch.nn as nn
import torch.nn.functional as F


class PixelShuffle(nn.Module):
    def __init__(self, downscale_factor):
        super().__init__()
        self.downscale_factor = downscale_factor

    def forward(self, x):
        bs, c, h, w = x.size()
        out_channel = c // (self.downscale_factor ** 2)
        out_h = h * self.downscale_factor
        out_w = w * self.downscale_factor
        x_view = x.contiguous().view(bs, out_channel, h, self.downscale_factor,
                                     w, self.downscale_factor)
        x_prime = x_view.permute(0, 1, 3, 5, 2, 4).contiguous().view(
            bs, out_channel, out_h, out_w)
        return x_prime


class UpScalingBlock(nn.Module):
    def __init__(self, in_channels=64):
        super().__init__()
        self.body = nn.Sequential(
            nn.Conv2d(in_channels, 64 * 4, 3, 1, 1),
            # nn.UpsamplingNearest2d(scale_factor=2),
            nn.PixelShuffle(2),
            # PixelShuffle(2),
            nn.Conv2d(64, 3, 3, 1, 1)
        )

    def forward(self, x):
        out = self.body(x)
        return out


class ResidualBlock(nn.Module):
    def __init__(self, channels=64):
        super().__init__()
        self.body = nn.Sequential(
            nn.Conv2d(channels, channels, 3, 1, 1),
            nn.ReLU(inplace=True),
            nn.Conv2d(channels, channels, 3, 1, 1),
        )

    def forward(self, x):
        out = self.body(x)
        out = out + x
        return out


class TAD_usn(nn.Module):
    def __init__(self):
        super().__init__()
        self.entry = nn.Conv2d(3, 64, 3, 1, 1)
        self.residuals = nn.Sequential(
            ResidualBlock(), ResidualBlock(), ResidualBlock())
        self.mid_conv = nn.Conv2d(64, 64, 3, 1, 1)
        self.exit = UpScalingBlock()

    def forward(self, x):
        b1 = self.entry(x)
        x = self.residuals(b1)
        x = self.mid_conv(x)
        out = self.exit(x + b1)
        return out
