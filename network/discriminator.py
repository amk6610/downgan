import torch
import torch.nn as nn


class DCGAN_D(nn.Module):
    def __init__(self, image_size=64, spectral=False, D_h_size=128, n_gpu=1):
        super().__init__()
        self.n_gpu = n_gpu
        main = nn.Sequential()
        main_conv = nn.Conv2d(3, D_h_size, kernel_size=4,
                              stride=2, padding=1, bias=False)
        if spectral:
            main_conv = nn.utils.spectral_norm(main_conv)
        main.add_module('Start-Conv2d', main_conv)
        main.add_module('Start-LeakyReLU', nn.LeakyReLU(0.2, inplace=False))
        image_size_new = image_size // 2
        mult = 1
        i = 0
        while image_size_new > 4:
            new_conv = nn.Conv2d(D_h_size * mult, D_h_size * (2 * mult),
                                 kernel_size=4, stride=2, padding=1, bias=False)
            if spectral:
                new_conv = nn.utils.spectral_norm(new_conv)
            main.add_module('Middle-Conv2d [%d]' % i, new_conv)
            main.add_module('Middle-LeakyReLU [%d]' %
                            i, nn.LeakyReLU(0.2, inplace=True))
            image_size_new = image_size_new // 2
            mult *= 2
            i += 1
        tail_conv = nn.Conv2d(D_h_size * mult, 1,
                              kernel_size=4, stride=1, padding=0, bias=False)
        if spectral:
            tail_conv = nn.utils.spectral_norm(tail_conv)
        main.add_module('End-Conv2d', tail_conv)
        # main.add_module('Sigmoid', nn.Sigmoid())
        self.main = main

    def forward(self, input):
        if isinstance(input.data, torch.cuda.FloatTensor) and self.n_gpu > 1:
            output = nn.parallel.data_parallel(
                self.main, input, range(self.n_gpu))
        else:
            # for item in self.main:
            #     print(input.shape)
            #     input = item(input)
            output = self.main(input)
        return output.view(-1)


class Discriminator(nn.Module):
    def __init__(self, ngpu=1):
        super(Discriminator, self).__init__()
        ndf = 128
        self.ngpu = ngpu
        self.main = nn.Sequential(
            # input is (nc) x 64 x 64
            nn.Conv2d(3, ndf, 4, 2, 1, bias=False),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf) x 32 x 32
            nn.Conv2d(ndf, ndf * 2, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ndf * 2),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf*2) x 16 x 16
            nn.Conv2d(ndf * 2, ndf * 4, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ndf * 4),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf*4) x 8 x 8
            nn.Conv2d(ndf * 4, ndf * 8, 4, 2, 1, bias=False),
            nn.BatchNorm2d(ndf * 8),
            nn.LeakyReLU(0.2, inplace=True),
            # state size. (ndf*8) x 4 x 4
            nn.Conv2d(ndf * 8, 1, 4, 1, 0, bias=False),

        )

    def forward(self, input):
        return self.main(input)
