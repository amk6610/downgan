import torch
import fid
from tqdm import tqdm
import torch.nn.functional as F
import torch.nn as nn
import loss.recon_loss as rec_loss

generated_lr_images_path = 'generated_lr_images.pt'
lr_images_path = 'lr_images.pt'
fid_batch_size = 8
fid_dims = 64
epoch_limit = 10


class Trainer:
    def __init__(self, down_sampling_network, up_sampling_network,
                 lr_discriminator, perceptual_loss, gen_optimizer, disc_optimizer,
                 scheduler, disc_scheduler, train_data_loader, val_data_loader, device, lr_l1_loss, gen_loss, disc_loss, recon_loss, gan_enabled=False, perceptual_enabled=False):
        super().__init__()
        self.down_sampling_network = down_sampling_network
        self.up_sampling_network = up_sampling_network
        self.lr_discriminator = lr_discriminator
        self.perceptual_loss = perceptual_loss
        self.gen_optimizer = gen_optimizer
        self.disc_optimizer = disc_optimizer
        self.scheduler = scheduler
        self.train_data_loader = train_data_loader
        self.val_data_loader = val_data_loader
        self.device = device
        self.lr_l1_loss = lr_l1_loss
        self.gen_loss = gen_loss
        self.disc_loss = disc_loss
        self.recon_loss = recon_loss
        self.gan_enabled = gan_enabled
        self.perceptual_enabled = perceptual_enabled
        self.disc_scheduler = disc_scheduler
        self.l1_co = 1
        self.gan_co = 0
        self.us_co = 1
        self.calc_psnr = rec_loss.ReconLoss(mode='psnr')

    def _step(self, is_train, epoch):
        torch.set_grad_enabled(is_train)
        self.down_sampling_network.train(is_train)
        self.up_sampling_network.train(is_train)
        self.lr_discriminator.train(is_train)
        total_loss = 0
        avg_psnr = 0

        for idx, (lr_img, hr_img) in enumerate(tqdm(self.train_data_loader if is_train else self.val_data_loader)):
            if epoch >= epoch_limit:
                # self.l1_co = 1
                self.gan_co = 3
                # self.us_co = 1
            lr_img = lr_img.to(self.device)
            hr_img = hr_img.to(self.device)
            lr_img_pool = F.avg_pool2d(hr_img, 2, 2)
            if idx % 2 == 0 or not is_train or not self.gan_enabled or epoch < epoch_limit:
                for p in self.lr_discriminator.parameters():
                    p.requires_grad = False
                lr_img_f = self.down_sampling_network(hr_img)
                # lr_img_f = self.down_sampling_network(lr_img)
                hr_img_recon = self.up_sampling_network(lr_img_f)
                us_loss = self.recon_loss(hr_img, hr_img_recon)
                l1_loss = self.lr_l1_loss(lr_img_pool, lr_img_f)
                # l1_loss = self.lr_l1_loss(lr_img, lr_img_f)
                perceptual_loss = 0
                if self.perceptual_enabled:
                    perceptual_loss = self.perceptual_loss(
                        lr_img_f, lr_img, hr_img)
                g_gan_loss = 0
                if self.gan_enabled and epoch >= epoch_limit:
                    with torch.no_grad():
                        fake_score = self.lr_discriminator(lr_img_f)
                    real_score = self.lr_discriminator(lr_img_pool)
                    g_gan_loss = self.gen_loss(real_score, fake_score)
                dsn_loss = (perceptual_loss + self.gan_co * g_gan_loss +
                            self.us_co * us_loss + self.l1_co * l1_loss) / (self.gan_co + self.l1_co + self.us_co)
                # print(perceptual_loss, g_gan_loss, us_loss)
                if is_train:
                    self.gen_optimizer.zero_grad()
                    dsn_loss.backward()
                    self.gen_optimizer.step()
                print(f'{idx}. batch generator loss:',
                      g_gan_loss, l1_loss)
                total_loss += dsn_loss.item()
            if is_train and self.gan_enabled:
                for p in self.lr_discriminator.parameters():
                    p.requires_grad = True
                self.disc_optimizer.zero_grad()
                with torch.no_grad():
                    lr_img_f = self.down_sampling_network(hr_img)
                    # lr_img_f = self.down_sampling_network(lr_img)
                fake_score = self.lr_discriminator(lr_img_f)
                real_score = self.lr_discriminator(lr_img_pool)
                # real_score = self.lr_discriminator(lr_img)
                d_gan_loss = self.disc_loss(
                    real_score, fake_score, lr_img_pool, lr_img_f)
                d_gan_loss.backward()
                self.disc_optimizer.step()
                print(f'{idx}. batch discriminator loss:', d_gan_loss.item())
            psnr = self.calc_psnr(lr_img_f, lr_img_pool)
            avg_psnr += psnr
            print(f'{idx}. PNSR:', psnr)

        return avg_psnr / (idx + 1), total_loss / (idx + 1)

    def step(self, epoch):
        train_psnr, train_loss = self._step(True, epoch)
        val_psnr, val_loss = self._step(False, epoch)
        # print('fid: {}'.format(self.validation()))
        self.scheduler.step()
        self.disc_scheduler.step()
        return val_psnr, val_loss, train_psnr, train_loss

    def validation(self):
        if not self.gan_enabled:
            return 0
        generated_lr_images = []
        lr_images = []
        while True:
            for idx, (lr_img, hr_img) in enumerate(self.val_data_loader):
                lr_img = lr_img.to(self.device)
                hr_img = hr_img.to(self.device)
                lr_img_gen = self.down_sampling_network(hr_img)
                # lr_img_gen = self.down_sampling_network(lr_img)
                generated_lr_images.append(lr_img_gen)
                lr_images.append(lr_img)
            if len(generated_lr_images) * generated_lr_images[0].size(0) >= fid_dims:
                break
        return fid.calculate_fid_given_paths(
            [torch.cat(generated_lr_images, 0), torch.cat(lr_images, 0)], fid_batch_size, 1, fid_dims)

    def sample(self, data_loader):
        with torch.no_grad():
            for idx, (lr_img, hr_img) in enumerate(data_loader):
                hr_img = hr_img.to(self.device)
                lr_img = lr_img.to(self.device)
                lr_img_f = self.down_sampling_network(hr_img)
                # lr_img_f = self.down_sampling_network(lr_img)
                return hr_img, lr_img_f, self.up_sampling_network(lr_img_f)
                # return hr_img, lr_img_f, lr_img
