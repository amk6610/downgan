import matplotlib.pyplot as plt
import numpy as np

result_dir = 'results/'


def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)


def show(img, i, j):
    npimg = img.cpu().numpy()
    plt.imshow(np.transpose(npimg, (1, 2, 0)), interpolation='nearest')
    plt.savefig(result_dir + str(i) + '_' + str(j) + '.jpg')
    # plt.show()
