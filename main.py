import torch
import torch.nn as nn
import torch.optim as optim
import numpy as np
from torchvision.datasets import ImageFolder
from torch.utils import data
from torchvision.utils import make_grid
from torchvision.transforms import ToTensor, Compose, Resize, RandomCrop, ToPILImage, Normalize, CenterCrop
from PIL import Image
import os
from random import shuffle
from math import floor

import network.discriminator as discriminator
import network.carn_m as down_sampler
import network.up_sampler as up_sampler
import network.tad_dsn as tad_down
import network.tad_usn as tad_up

import loss.perceptual_loss as p_loss
import loss.gan_loss as gan_loss
import loss.recon_loss as rec_loss
from trainer import Trainer
import matplotlib.pyplot as plt
from torchvision.transforms import functional as F
import copy
import utils

dataset = 'div2k'

train_img_name = '00003.JPG'
train_img_lr_name = '00005.JPG'
root_dir = 'dataset/'
val_train_ratio = 0.1

mode = 'lsgan'
grad_penalty_lambda = 10
betas = (0.5, 0.999)
gamma = 0.33
step_size = 20
batch_size = 16
num_epochs = 50


# class PairRandomCrop(RandomCrop):
#     def __call__(self, img1, img2):
#         """
#         Args:
#             img (PIL Image): Image to be cropped.

#         Returns:
#             PIL Image: Cropped image.
#         """
#         if self.padding is not None:
#             img1 = F.pad(img1, self.padding, self.fill, self.padding_mode)
#             img2 = F.pad(img2, self.padding, self.fill, self.padding_mode)

#         # pad the width if needed
#         if self.pad_if_needed and img1.size[0] < self.size[1]:
#             img1 = F.pad(
#                 img1, (self.size[1] - img1.size[0], 0), self.fill, self.padding_mode)
#         if self.pad_if_needed and img2.size[0] < self.size[1]:
#             img2 = F.pad(
#                 img2, (self.size[1] - img2.size[0], 0), self.fill, self.padding_mode)
#         # pad the height if needed
#         if self.pad_if_needed and img1.size[1] < self.size[0]:
#             img1 = F.pad(
#                 img1, (0, self.size[0] - img1.size[1]), self.fill, self.padding_mode)
#         if self.pad_if_needed and img2.size[1] < self.size[0]:
#             img2 = F.pad(
#                 img2, (0, self.size[0] - img2.size[1]), self.fill, self.padding_mode)

#         i, j, h, w = self.get_params(img1, self.size)

#         return F.crop(img1, i, j, h, w), F.crop(img2, i, j, h, w)


def crop(image, ratio):
    width, height = image.size[0], image.size[1]
    new_width = width * ratio
    new_height = height * ratio
    # print(new_width, new_height, width, height,
    #       (width - new_width)/2., (height - new_height)/2.)
    left = np.ceil((width - new_width)/2.)
    top = np.ceil((height - new_height)/2.)

    return F.crop(image, top, left, new_height, new_width)
    # print("Cropping boundary: ", top, bottom, left, right)


class Dataset(data.Dataset):
    def __init__(self, dir):
        self.dir = dir
        common_transforms = [ToTensor(), Normalize(
            (0.5, 0.5, 0.5), (0.5, 0.5, 0.5))]
        self.hr_transform = Compose(
            [RandomCrop((128, 128)), *common_transforms])
        self.lr_transform = Compose(
            [RandomCrop((64, 64)), *common_transforms])
        self.shuffled_dir = copy.deepcopy(dir)
        shuffle(self.shuffled_dir)
        self.data = {}

    def __len__(self):
        return len(self.dir)

    def __getitem__(self, index):
        if index in self.data.keys():
            return self.data[index]
        else:
            # index = 0
            hr_root = self.dir[index]
            lr_root = self.shuffled_dir[index]

            # print(root)
            hr = Image.open(hr_root + '/' + train_img_name)
            # hr = self.transform(hr)
            lr = Image.open(lr_root + '/' + train_img_lr_name)
            # lr = crop(lr, 0.5)
            # if '00022' in root:
            #     hr.show()
            #     lr.show()
            hr = self.hr_transform(hr)
            lr = self.lr_transform(lr)
            self.data[index] = (lr, hr)

            # print(hr.size(), lr.size())

            # lr = self.transform(lr)
            # lr = crop(lr, 0.5)
            return lr, hr


class DIV2K_dataset(data.Dataset):
    def __init__(self, dir):
        self.dir = dir
        self.hr_transform = Compose(
            [RandomCrop((128, 128)), ToTensor(), Normalize(
                (0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
        self.shuffled_dir = copy.deepcopy(dir)
        shuffle(self.shuffled_dir)
        self.data = {}

    def __len__(self):
        return len(self.dir)

    def __getitem__(self, index):
        if index in self.data.keys():
            return self.data[index]
        else:
            root = self.dir[index]
            img = Image.open(root)
            img = self.hr_transform(img)
            self.data[index] = (img, img)
            return self.data[index]


def get_dir_names(root_dir, train_dir, val_dir, dataset='zoom'):
    data_directories = ([], [])
    for idx, directory in enumerate([train_dir, val_dir]):
        for root, dirs, files in os.walk(root_dir + directory, topdown=False):
            if dataset == 'zoom':
                for name in dirs:
                    if 'aligned' not in name:
                        data_directories[idx].append(os.path.join(root, name))
            elif dataset == 'div2k':
                for file in files:
                    data_directories[idx].append(os.path.join(root, file))
    return data_directories
    # data_size = len(data_directories)
    # shuffle(data_directories)
    # val_size = floor(data_size * val_train_ratio)
    # return data_directories[val_size:], data_directories[:val_size]


def main():
    # initialization
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    # down_sampling_network = tad_down.TAD_dsn().to(device)
    down_sampling_network = tad_down.TAD_dsn().to(device)
    up_sampling_network = tad_up.TAD_usn().to(device)
    lr_discriminator = discriminator.DCGAN_D().to(
        device)
    perceptual_loss = p_loss.PerceptualLoss().to(device)
    gen_optimizer = optim.Adam(
        list(down_sampling_network.parameters()) + list(up_sampling_network.parameters()), lr=3e-4, betas=betas)
    disc_optimizer = optim.Adam(
        lr_discriminator.parameters(), lr=3e-4, betas=betas)
    scheduler = optim.lr_scheduler.StepLR(
        gen_optimizer, step_size=step_size, gamma=gamma)

    disc_scheduler = optim.lr_scheduler.StepLR(
        disc_optimizer, step_size=step_size, gamma=gamma)

    disc_loss, gen_loss = gan_loss.get_gan_losses(
        mode, grad_penalty_lambda, lr_discriminator)
    recon_loss = rec_loss.ReconLoss(mode='l2')

    # train_dirs, val_dirs = get_dir_names(root_dir, 'train', 'test')
    train_dirs, val_dirs = get_dir_names(
        root_dir + 'DIV2K/', 'DIV2K_train_HR', 'DIV2K_valid_HR', dataset=dataset)

    # print(train_dirs, val_dirs)
    # exit(0)

    train_dataset_loader = Dataset(dir=train_dirs)
    val_dataset_loader = Dataset(dir=val_dirs)
    if dataset == 'div2k':
        train_dataset_loader = DIV2K_dataset(dir=train_dirs)
        val_dataset_loader = DIV2K_dataset(dir=val_dirs)

    train_loader = data.DataLoader(
        train_dataset_loader,
        batch_size=batch_size,
        num_workers=4,
        shuffle=False
    )
    val_loader = data.DataLoader(
        val_dataset_loader,
        batch_size=batch_size,
        num_workers=4,
        shuffle=False
    )

    print('num_parms:', utils.count_parameters(down_sampling_network) +
          utils.count_parameters(up_sampling_network))

    trainer = Trainer(down_sampling_network, up_sampling_network, lr_discriminator, perceptual_loss,
                      gen_optimizer, disc_optimizer, scheduler, disc_scheduler, train_loader, val_loader, device, recon_loss, gen_loss, disc_loss, recon_loss, gan_enabled=True)

    train_epoch_loss = []
    val_epoch_loss = []
    # for x, y in val_loader:
    #     continue
    # exit(0)
    for i in range(num_epochs):
        val_psnr, val_loss, train_psnr, train_loss = trainer.step(i)
        sample = trainer.sample(train_loader)
        # if i != 0 and i % 10 == 0:
        for j in range(3):
            # print(sample[j].mean().item(),
                #   sample[j].min(), sample[j].max())
            utils.show(make_grid(sample[j], scale_each=True), i, j)

        if i % 10 == 0:
            print('fid:', trainer.validation())
        print(f'[{i + 1}] train_loss: {train_loss} train_psnr: {train_psnr} val_loss: {val_loss} val_psnr: {val_psnr}')
        train_epoch_loss.append(train_loss)
        val_epoch_loss.append(val_loss)

    plt.figure()
    plt.plot([i + 1 for i in range(num_epochs)], train_epoch_loss, marker='o')
    plt.plot([i + 1 for i in range(num_epochs)], val_epoch_loss, marker='o')
    plt.savefig('loss.jpg')
    plt.show()

    # class CustomImageFolder(ImageFolder):
    #     def __getitem__(self, index):
    #         filename = self.imgs[index]
    #         print(filename)
    #         return super().__getitem__(index)

    # train_dataset = CustomImageFolder(
    #     root='dataset/test/',
    #     transform=ToTensor()
    # )

    # print(train_loader[0])

    # for hr, lr in train_loader:
    #     # print(np.array(img).shape, label)
    #     print(hr.size(), lr.size())


if __name__ == '__main__':
    main()
