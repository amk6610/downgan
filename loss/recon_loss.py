import torch
import torch.nn as nn
import torch.nn.functional as F
from math import log10


class ReconLoss(nn.Module):
    def __init__(self, mode='l1'):
        super().__init__()
        assert mode in ('l1', 'l2', 'ms-ssim', 'psnr')
        self.mode = mode

    def forward(self, hr, hr_pred):
        if self.mode == 'l1':
            return torch.abs(hr - hr_pred).mean()
        if self.mode == 'l2':
            return F.mse_loss(hr_pred, hr)
        if self.mode == 'ms-ssim':
            raise NotImplementedError()
        if self.mode == 'psnr':
            criterion = nn.MSELoss().to(hr)
            mse = criterion(hr, hr_pred)
            return 10 * log10(1 / mse.item())
