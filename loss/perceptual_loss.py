import torch
import torch.nn as nn
import torch.nn.functional as F
from torchvision.models import vgg19


def _preprocess(img):
    mean = torch.tensor([0.485, 0.456, 0.406]).to(img).view(1, -1, 1, 1)
    std = torch.tensor([0.229, 0.224, 0.225]).to(img).view(1, -1, 1, 1)
    return F.interpolate((img - mean) / std, size=224, mode='bilinear', align_corners=False)


class PerceptualLoss(nn.Module):
    def __init__(self, last=True):  # TODO rename this argument
        super().__init__()
        self.vgg = vgg19(True)
        self.vgg.classifier = nn.Identity()
        self.last = last

    def forward(self, lr, hr, hr_rec):
        hr_features = self.vgg(_preprocess(hr))
        if self.last:
            pred_features = self.vgg(_preprocess(hr_rec))
        else:
            pred_features = self.vgg(_preprocess(lr))
        return torch.pow(hr_features - pred_features, 2).mean()
