import torch
import torch.nn as nn
from torch.autograd import Variable


def disc_loss_with_penalty(base_loss_func, grad_penalty_lambda, discriminator):
    def disc_loss(real, fake, x_real, x_fake):
        base_loss = base_loss_func(real, fake)
        if grad_penalty_lambda == 0:
            return base_loss
        return base_loss + grad_penalty_lambda * calc_grad_penalty(x_real, x_fake, discriminator)

    return disc_loss


def get_gan_losses(mode, grad_penalty_lambda, discriminator):
    if mode == 'jsgan':
        d_loss, g_loss = get_js_gan_losses()
    elif mode == 'lsgan':
        d_loss, g_loss = get_ls_gan_losses()
    elif mode == 'wgan':
        d_loss, g_loss = get_w_gan_losses()
    elif mode == 'hinge':
        d_loss, g_loss = get_hinge_gan_losses()
    elif mode == 'rsgan':
        d_loss, g_loss = get_rs_gan_losses()
    elif mode == 'rasgan':
        d_loss, g_loss = get_ras_gan_losses()
    elif mode == 'ralsgan':
        d_loss, g_loss = get_rals_gan_losses()
    elif mode == 'rahinge':
        d_loss, g_loss = get_rahinge_gan_losses()
    else:
        raise ValueError('Invalid gan loss type')
    return disc_loss_with_penalty(d_loss, grad_penalty_lambda, discriminator), g_loss


def _get_ones(tensor):
    return torch.ones(tensor.size(0)).to(tensor)


def _get_zeros(tensor):
    return torch.zeros(tensor.size(0)).to(tensor)


def calc_grad_penalty(x_real, x_fake, discriminator):
    # print(x_real.size(), x_fake.size())
    u = torch.rand(x_real.size(0), 1, 1, 1).to(x_real)  # random
    x_both = x_real.data * u + x_fake.data * (1 - u)
    # We only want the gradients with respect to x_both
    x_both = Variable(x_both, requires_grad=True)
    # print(discriminator(x_both).size())
    grad = torch.autograd.grad(outputs=discriminator(x_both), inputs=x_both, grad_outputs=_get_ones(x_real),
                               retain_graph=True, create_graph=True, only_inputs=True)[0]
    # We need to norm 3 times (over n_colors x image_size x image_size) to get only a vector of size "batch_size"
    return ((grad.norm(2, 1).norm(2, 1).norm(2, 1) - 1) ** 2).mean()


def get_js_gan_losses():
    criterion = nn.BCEWithLogitsLoss()

    def disc_loss(real, fake):
        return criterion(real, _get_ones(real)) + criterion(fake, _get_zeros(fake))

    def gen_loss(_, fake):
        return criterion(fake, _get_ones(fake))

    return disc_loss, gen_loss


def get_ls_gan_losses():
    def disc_loss(real, fake):
        return torch.mean((real - _get_ones(real)) ** 2) + torch.mean(fake ** 2)

    def gen_loss(_, fake):
        return torch.mean((fake - _get_ones(fake)) ** 2)

    return disc_loss, gen_loss


def get_w_gan_losses():
    def disc_loss(real, fake):
        return -torch.mean(real) + torch.mean(fake)

    def gen_loss(_, fake):
        return -torch.mean(fake)

    return disc_loss, gen_loss


def get_hinge_gan_losses():
    def disc_loss(real, fake):
        return torch.mean(nn.ReLU()(1.0 - real)) + torch.mean(nn.ReLU()(1.0 + fake))

    def gen_loss(_, fake):
        return -torch.mean(fake)

    return disc_loss, gen_loss


def get_rs_gan_losses():
    criterion = nn.BCEWithLogitsLoss()

    def disc_loss(real, fake):
        return criterion(real - fake, _get_ones(real))

    def gen_loss(real, fake):
        return criterion(fake - real, _get_ones(fake))

    return disc_loss, gen_loss


def get_ras_gan_losses():
    criterion = nn.BCEWithLogitsLoss()

    def disc_loss(real, fake):
        return (criterion(real - torch.mean(fake), _get_ones(real)) +
                criterion(fake - torch.mean(real), _get_zeros(fake))) / 2

    def gen_loss(real, fake):
        return (criterion(real - torch.mean(fake), _get_zeros(real)) +
                criterion(fake - torch.mean(real), _get_ones(fake))) / 2

    return disc_loss, gen_loss


def get_rals_gan_losses():
    criterion = nn.BCEWithLogitsLoss()

    def disc_loss(real, fake):
        return (torch.mean((real - torch.mean(fake) - _get_ones(real)) ** 2) +
                torch.mean((fake - torch.mean(real) + _get_ones(fake)) ** 2)) / 2

    def gen_loss(real, fake):
        return (torch.mean((real - torch.mean(fake) + _get_ones(real)) ** 2) +
                torch.mean((fake - torch.mean(real) - _get_ones(fake)) ** 2)) / 2

    return disc_loss, gen_loss


def get_rahinge_gan_losses():
    def disc_loss(real, fake):
        return (torch.mean(nn.ReLU()(1.0 - (real - torch.mean(fake)))) +
                torch.mean(nn.ReLU()(1.0 + (fake - torch.mean(real))))) / 2

    def gen_loss(real, fake):
        return (torch.mean(nn.ReLU()(1.0 + (real - torch.mean(fake)))) +
                torch.mean(nn.ReLU()(1.0 - (fake - torch.mean(real))))) / 2

    return disc_loss, gen_loss
